import java.util.*;

public class LongStack {

    public static void main(String[] argum) {
        // TODO!!! Your tests here!
    }

    private final LinkedList<Long> stack;

    LongStack() {
        stack = new LinkedList<>();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        LongStack clone = new LongStack();
        Collections.reverse(stack);
        for (Long element : stack) {
            clone.push(element);
        }
        Collections.reverse(stack);
        return clone;
    }

    public boolean stEmpty() {
        return stack.size() == 0;
    }

    public void push(long a) {
        stack.add(0, a);
    }

    public long pop() {
        if (stack.size() == 0) {
            throw new IndexOutOfBoundsException("stack is empty");
        }
        Long first = stack.getFirst();
        stack.removeFirst();
        return first;
    }

    public void op(String s) {
        try {
            Long first = stack.pop();
            Long second = stack.pop();
            if ("+".equals(s)) {
                stack.push(second + first);
            } else if ("-".equals(s)) {
                stack.push(second - first);
            } else if ("*".equals(s)) {
                stack.push(second * first);
            } else if ("/".equals(s)) {
                stack.push(second / first);
            } else {
                throw new IllegalArgumentException("Invalid operation: " + s);
            }
        } catch (IndexOutOfBoundsException e) {
            throw new IndexOutOfBoundsException("Stack is empty!");
        }
    }

    public long tos() {
        if (stack.size() == 0) {
            throw new IndexOutOfBoundsException("Stack is empty!");
        }
        return stack.getFirst();
    }

    private int size() {
        return stack.size();
    }

    private Long get(int index) {
        return stack.get(index);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof LongStack)) {
            return false;
        }
        LongStack c = (LongStack) o;
        if (c.size() != stack.size()) {
            return false;
        }
        int counter = 0;
        for (Long element : stack) {
            if (!element.equals(c.get(counter))) {
                return false;
            }
            counter++;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder answer = new StringBuilder();
        if (stack.size() == 0) {
            return answer.toString();
        }
        for (Long element : stack) {
            answer.insert(0, element.toString() + " ");
        }
        return answer.toString();
    }

    public static long interpret(String expression) {
        if (expression.length() == 0) {
            throw new IllegalArgumentException("Expression can not be empty!");
        }
        String operators = "+-*/";

        LongStack stack = new LongStack();
        String[] splitted = expression.split(" ");

        for (String s : splitted) {
            s = s.trim();
            if (s.equals("")) {
                continue;
            }
            if (!operators.contains(s)) {
                try {
                    stack.push(Long.parseLong(s));
                } catch (NumberFormatException e) {
                    throw new IllegalArgumentException("Illegal symbol " + s + " in expression: " + expression);
                }

            } else {
                try {
                    stack.op(s);
                } catch (NoSuchElementException e) {
                    throw new IllegalArgumentException("Can not perform " + s + " in expression: " + expression);
                }
            }
        }
        if (stack.size() > 1) {
            throw new IllegalArgumentException("Too many numbers in expression: " + expression);
        }
        return stack.pop();
    }
}
